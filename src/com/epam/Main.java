package com.epam;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //-------Task1-------//
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter string to reverse");
        String str = sc.nextLine();
        System.out.println("Reversed string is: " + reverseStr(str));
        //-------Task2-------//
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Please enter day of a week: ");
        String cl = sc1.next();
        DayOfWeek dayOfWeek = DayOfWeek.valueOf(cl.toUpperCase());
        System.out.println("The day what you want is: " + getTimeToNextDay());
        System.out.println("The day what you want is: " + getTimeToNextDay(dayOfWeek.ordinal()+1));
    }

    //-------Method for Task1-------//
    public static String reverseStr(String str) {
        char[] ch = new char[str.length()];
        for (int i = ch.length - 1; i >= 0; i--) {
            ch[i] = str.toCharArray()[ch.length - i - 1];
        }
        return String.valueOf(ch);
    }

    //-------Overload Method for Task2-------//
    public static String getTimeToNextDay() {
        Calendar now = Calendar.getInstance();
        int dayOfWeek = Calendar.MONDAY;
        int weekday = now.get(Calendar.DAY_OF_WEEK);
        int NextDayOfWeek = dayOfWeek - weekday;
        if (NextDayOfWeek < 0) NextDayOfWeek += 7;
        now.add(Calendar.DAY_OF_YEAR, NextDayOfWeek);
        DateFormat df = new SimpleDateFormat("EEEE, dd MMM yyy");
        return df.format(now.getTime());
    }

    //-------Method for Task2-------//
    public static String getTimeToNextDay(int dayOfWeek) {
        Calendar now = Calendar.getInstance();
        int weekday = now.get(Calendar.DAY_OF_WEEK);
        int NextDayOfWeek = dayOfWeek - weekday;
        if (NextDayOfWeek < 0) NextDayOfWeek += 7;
        now.add(Calendar.DAY_OF_YEAR, NextDayOfWeek);
        DateFormat df = new SimpleDateFormat("EEEE, dd MMM yyy");
        return df.format(now.getTime());
    }

}

